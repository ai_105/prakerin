<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sample = [ 
            ['title' => 'belajar huruf hijaiyah',
                'content' => 'loren ipsum sit amet dolar'],
            ['title' => 'indonesian U-19 gagal lolos semifinal',
              'content' => 'loren ipsum sit amet dolar'],
            ['title' => 'cara cepet belajar pemrograman',
               'content' => 'try & error terus menerus']
        ];

        DB::table('posts')->insert($sample);
    }
}
