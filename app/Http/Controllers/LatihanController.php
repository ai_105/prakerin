<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LatihanController extends Controller
{
    public function perkenalan()
    {
        $nama = $nama1;
        $alamat = $alamat1;
        $umur = $umur1;

        return view('pages.perkenalan', compact('nama','alamat','umur'));
    }

    public function perkenalan2($nama,$umur,$alamat){

    }

    public function siswa()
    {
    $a = [
        array('id' => 1, 'name' => 'Dadang', 'age' => 15, 'hobi' => [
            'mancing', 'futsal', 'renang',
    ]),
        array('id' => 2, 'name' => 'Dudung', 'age' => 18, 'hobi' => [
            'baca buku', 'bernyanyi',
        ]),
    ];
    // dd($a);
    return view('pages.siswa', ['siswa' => $a]);
    }

    public function skripsi()
    {
    $dosen = [
        ['nama' => 'Yusuf Bachtiar', 'mata_kuliah' => 'Program Mobile','mahasiswa' => [  
            ['nama' => 'Muhammad Soleh ', 'nilai' => 78],
            ['nama' => 'Jujun Junaedi', 'nilai' => 85],
            ['nama' => 'Mamat Alkatiri', 'nilai' => 90],
        ],
        ],
        ['nama' => 'Yariz Riyadi', 'mata_kuliah' => 'Pemrograman ','mahasiswa' => [
            ['nama' => 'Alfred McTomminay', 'nilai' => 67],
            ['nama' => 'Bruno Kasmir', 'nilai' => 90],
        ]
        ],
    ];
    // dd($a);
    return view('pages.dosen', compact('dosen'));
    }

    public function stasiun()
    {
        $tv = [
            array('tv' => 'net tv', 'jam' => '19.00', 'tgl' => '14 july 2022', 'acara' => 'keluarga'),

            array('tv' => 'tvri', 'jam' => '07.30', 'tgl' => '14 july 2022', 'acara' => 'musik'),

            array('tv' => 'beinsport', 'jam' => '19.00', 'tgl' => '14 july 2022', 'acara' => 'keluarga'),

            array('tv' => 'Ochanel', 'jam' => '07.30', 'tgl' => '14 july 2022', 'acara' => 'musik'),
            
            array('tv' => 'indosiar', 'jam' => '18.00', 'tgl' => '14 july 2022', 'acara' => 'musik dangdut'),
        ];
    // dd($a);
    return view('pages.stasiun', compact('tv'));
    }

public function shop(){
    $belanja = [
        ['nama_orang'=>'ALFIAN',
        'pembelian'=>
      [
        ['jenis' => 'sepatu', 'merk' => 'Vans', 'harga'=>250000],
        ['jenis' => 'baju', 'merk' => 'Erigo', 'harga'=>250000],
        ['jenis' => 'celana', 'merk' => 'Erigo', 'harga'=>250000],
        ['jenis' => 'kupluk', 'merk' => 'Vans', 'harga'=>250000]
      ]  
    ],
    ['nama_orang'=>'DIDA',
    'pembelian'=>
    [
    ['jenis' => 'sepatu', 'merk' => 'Vans', 'harga'=>250000],
    ['jenis' => 'baju', 'merk' => 'Erigo', 'harga'=>250000],
    ['jenis' => 'celana', 'merk' => 'Erigo', 'harga'=>250000] 
    ]
    ]
];
return view('pages.shop', ['belanja'=>$belanja]);
}

public function grade(){
    $grade = [
        ['nama' => 'AGUS', 'jurusan' => 'TKRO','GRADE' => [  
            ['indonesia' => 80],
            ['ingris' => 97],
            ['produktif' => 67],
            ['mtk' => 100]
        ],
        ],
        ['nama' => 'AGUS', 'jurusan' => 'TKRO','GRADE' => [  
            ['indonesia' => 80],
            ['ingris' => 97],
            ['produktif' => 67],
            ['mtk' => 100]
        ],
        ],
        ['nama' => 'AGUS', 'jurusan' => 'TKRO','GRADE' => [  
            ['indonesia' => 80],
            ['ingris' => 97],
            ['produktif' => 67],
            ['mtk' => 100]
        ],
        ],
    ];
    // dd($a);
    return view('pages.grade', ['grade'=>$grade]);
}

}