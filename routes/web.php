<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LatihanController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route basic
Route :: get('/about', function(){ 
    return view('tentang');
});

Route::get('profile', function(){
    $nama = "Ai Romania";
    return view('pages.profile', compact('nama',));
});

Route::get('biodata', function(){
    $nama = "Ai Romania";
    $kelas = "XII RPL 1";
    $umur = "17 tahun";
    $tgl = "26 september 2004";
    $jk = "perempuan";
    return view('pages.biodata', compact('nama','kelas','umur','tgl','jk'));
});

// route parameter
Route:: get('biodata1/{nama}', function ($a) {
    return view('pages.bio', compact('a'));
});

Route:: get('order/{makanan}/{minuman}/{harga}', function ($a,$b,$c) {
    return view('pages.order', compact('a','b','c'));
});

// route optional parameter
Route::get('pesan/{menu}', function ($a = "-"){
    return view('pages.pesan', compact('a'));
});

// pemanggilan route dengan controller
Route::get('Latihan/{nama1}/{alamat1}/{umur1}', [LatihanController::class, 'perkenalan']);

Route::get('data', [LatihanController::class, 'siswa']);

Route::get('mahasiswa', [LatihanController::class, 'skripsi']);

Route::get('soal', [LatihanController::class, 'stasiun']);

Route::get('shop', [LatihanController::class, 'shop']);

Route::get('grade', [LatihanController::class, 'grade']);

//route post
Route::get('post', [PostController::class, 'tampil']);