<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <fieldset>
        <legend>GRADE NILAI</legend>
        @foreach ($jurusan as $grade)
        @php
            $total = 0;
        @endphp
        Nama Siswa :
        Nama : {{ $grade['nama'] }} <br>
        Jurusan    : {{ $grade['jurusan'] }} <br>
        Daftar Nilai :
        @foreach ($grade['siswa'] as $nilai)
        <li>{{ $nilai['nama'] }} <br>
            bahasa indonesia   : {{ $nilai['indonesia']}}
            bahasa inggris     : {{ $nilai['inggris']}}
            produktif kejuruan : {{ $nilai['produktif']}}
            matimatika         : {{ $nilai['mtk']}}
        </li>
        <hr>
    </fieldset>
</body>
</html>