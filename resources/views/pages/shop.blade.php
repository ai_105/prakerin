<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <fieldset>
        <legend>SHOPING</legend>
        @foreach ($belanja as $belanja)
        <hr>
        Nama Pembeli : {{ $belanja['nama_orang'] }} <br>
        --------------------------------
        <?php $total = 0; ?>
        @foreach ($belanja['pembelian'] as $item)
        <li> Jenis {{ $item['jenis'] }} <br> </li>
        - Merk {{ $item['merk'] }} <br>
        - Harga {{ $item['harga'] }} <br>
        <hr>
        <?php $total += $item['harga']?>
        Total Harga : {{ $total }} <br>
        @endforeach
        @if($total > 250000 && $total < 500000)
        @php
        $cashback = (5/100) * $total @endphp
        @elseif($total >=500)
        @php
        $cashback = (10/100) * $total @endphp
        @else
        @endif
            Cashback : {{ $cashback }}
        @endforeach
    </fieldset>
</body>
</html>